# Capstone I Group 5

**Backend**

- Dylan Hampton
- Timothy Sallwasser
- Anastasia Reed-Comeaux

**Frontend**

- Andrew Sabala
- Sudeep Potluri
- Caleb Arant


## Useful Links

- [Flask Documentation](https://flask.palletsprojects.com/en/3.0.x/)
- [Jinja Documentation](https://jinja.palletsprojects.com/en/3.1.x/) (Jinja is the template engine for Flask.)
- [W3Schools](https://www.w3schools.com) (references for CSS, HTML, JavaScript, SQL, etc.)
- [Trello Board](https://trello.com/invite/b/G5s9irBV/ATTI8c6d040208dc9ce1427e1f51e36501a05C09E634/tasks)(Task Board for Project)

## Useful Information

**Dependencies**:

- Flask (It may go by the name `python3-flask` in some Linux package manager repos.)
- black (A Python reformatter)
- mypy (optional, see below)

**Before pushing to the Git repo**:

Run `black`, the Python reformatter, on all changed Python files. This will keep all of the source code formatted consistently.

Depending on how much type safety we want to put into this application, running `mypy` might also be a good idea.

**Starting With Docker:**

First run `docker build -t test_app .` to create image

Then ` docker run -it -p 5000:5000 --tty --mount type=bind,source="$(pwd)"/,target=/app test_app fish` to launch server

Now everything should be available to host machine.

**To initialize the app's database:**

`cd` into the `flask_app` directory and execute `flask --app kahoot init-db`.

**To start the localhost debug web server:**

`cd` into the `flask_app` directory and execute `flask --debug --app kahoot run`.

## Timeline (subject to review)

- [X] Bare bones Kahoot by October 18
- [X] Bare bones graphics by start of November
- [ ] Base game by November 15

## Link to Presentation 
- [Presentation](https://mailmissouri-my.sharepoint.com/:p:/r/personal/tjsntr_umsystem_edu/Documents/Capstone%20I%20group%205/Capstone_I_Group_5.pptx?d=w6f0b4ed298d9470bb4fd0f011bc9a534&csf=1&web=1&e=mgjowc&nav=eyJzSWQiOjI1NiwiY0lkIjowfQ)
