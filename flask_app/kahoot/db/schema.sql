-- Drop tables if they exist here.
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS score_board;
DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS points;
DROP TABLE IF EXISTS quiz;
DROP TABLE IF EXISTS game;
DROP TABLE IF EXISTS score_board;
DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS points;

-- Create each table structure here.

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  passphrase TEXT NOT NULL
);

CREATE TABLE game (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(64) NOT NULL,
  owner_id INTEGER NOT NULL,
  num_display INTEGER NOT NULL DEFAULT 5,
  FOREIGN KEY (owner_id) REFERENCES session(id)
);

CREATE TABLE score_board(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  player INTEGER NOT NULL,
  points INTEGER NOT NULL,
  game_owner INTEGER NOT NULL,
  FOREIGN KEY (player) REFERENCES user(username),
  FOREIGN KEY (game_owner) REFERENCES user(id)
);
-- CREATE TABLE score_board(
--   id INTEGER PRIMARY KEY AUTOINCREMENT,
--   player INTEGER NOT NULL,
--   points INTEGER NOT NULL,
--   game_owner INTEGER NOT NULL,
--   FOREIGN KEY (player) REFERENCES user(username),
--   FOREIGN KEY (game_owner) REFERENCES user(id)
-- );

CREATE TABLE session (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  game_id INTEGER NOT NULL,
  owner_id INTEGER NOT NULL,
  --score_board_id INTEGER NOT NULL,
  --status VARCHAR(64) NOT NULL,
  questions LONGTEXT NOT NULL,
  answered LONGTEXT NOT NULL,
  --num_answers INTEGER NOT NULL DEFAULT 0,
  --num_players INTEGER NOT NULL DEFAULT 0,
  FOREIGN KEY (owner_id) REFERENCES user(id),
  FOREIGN KEY (game_id) REFERENCES game(id)--,
  --FOREIGN KEY (score_board_id) REFERENCES score_board(id)
);

-- ideas for statuses --
-- pre-launch -> waiting for students to join game
-- waiting -> waiting for answers (question should be showed) 
-- score-display -> showing the score board
-- reflect -> allowing teachers to answer any questions about the previous question
-- disabled -> session not happening right now

CREATE TABLE points(
  user_id INTEGER NOT NULL,
  game_id INTEGER NOT NULL,
  points INTEGER NOT NULL DEFAULT 0,
  FOREIGN KEY (user_id) REFERENCES user(id),
  FOREIGN KEY (game_id) REFERENCES game(id)
);
