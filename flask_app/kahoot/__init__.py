import os
from flask import Flask, redirect, render_template, url_for
from .routers import user

from kahoot.routers.auth import login_required


def create_app(test_config=None):
    # Create and configure the app.
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(app.instance_path, "flaskr.sqlite"),
    )

    if test_config is None:
        # Load the instance config, if it exists, when not testing.
        app.config.from_pyfile("config.py", silent=True)
    else:
        # Load the test config if passed in.
        app.config.from_mapping(test_config)

    # Ensure the instance folder exists.
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from .db import db

    with app.app_context():
        db.init_app(app)

    from .routers import auth, game

    app.register_blueprint(auth.bp)
    app.register_blueprint(user.bp)
    app.register_blueprint(game.bp)

    # Automatically route requests to the website's main address to the
    # registration page. The route should probably later be changed to the
    # login page.
    @app.route("/")
    def route_to_auth():
        return redirect(url_for("auth.login"))

    @app.route("/register")
    def route_to_register():
        return redirect(url_for("auth.register"))

    @app.route("/quiz_creator")
    def route_to_createQuiz():
        return redirect(url_for("user.quiz_editor"))

    @app.route("/home")
    @login_required
    def index():
        return render_template("Landingpage.html")

    return app
