// TODO:
// form validation
//  -> only allow nums that are in range of chosen options

const configs = document.getElementById("configsForm");
const questionType = document.getElementById("questionType");
const basicQuestionForm = document.getElementById("basicQuestionForm");
const openQuestionForm = document.getElementById("openQuestionForm");
const imageQuestionForm = document.getElementById("imageQuestionForm");
const contextForm = document.getElementById("contextForm");
const newQuestionBtn = document.getElementById("createQuestion");//to create new question
const addQuestionBtn = document.getElementById("addQuestion"); //to add question to quiz
const submitBtn = document.getElementById("submitBtn"); //for submit quiz
let forms = {
    "basicQuestionForm": basicQuestionForm, 
    "openQuestionForm": openQuestionForm,
    "imageQuestionForm": imageQuestionForm,
    "contextForm": contextForm};

forms.keys = ["basicQuestionForm", "openQuestionForm", "imageQuestionForm"];
const questions = {}; //store json formatted strings for easy convert to question class
let qIndex = 0;
let currForm = basicQuestionForm;

// basic form validation
function formValidation(formElements){
    for (elem of formElements){
        if (elem.required && elem.value == ""){
            alert("Missing info");
            elem.focus();
            return false;
        }
    }
    return true
}

function formToJson(formData, configsData) {
    const text = formData[0].value;
    let options = [];
    let ans = [];
    let optionCount = 0;
    let tmp = {
        "text": text
    }
    if (currForm.id == "imageQuestionForm"){
        if (document.getElementById("imageQuestion").files.length > 0){
            let textImg = formData["imageQuestion"].files[0];
            tmp["textImg"] = toDataURL(textImg);
        }
    }
    for (elem of formData){
        if (elem.name == "option" && elem.value != ""){
            if (currForm.id == "imageQuestionForm"){
                options.push(toDataURL(elem.files[0]));
            }else{
            options.push(elem.value);
            }
            optionCount += 1;
        }else if (elem.name == "answer"){
            ans = elem.value.replaceAll(",", " ").split(' ');
        }
    }
    //for (elem of configsData){
    //    tmp[elem.name] = elem.value;
    //}
    if (ans.length > 0){
        tmp["answer"] = ans
    }
    if (options.length > 0){
        tmp["options"] = options
    }
    const name = "question" + (qIndex+1).toString();
    questions[name] = tmp;
    qIndex += 1;
}

// supposed to convert images to base64 for storage in quiz file but doesn't work (reader.results is empty)
function toDataURL(){
    const file = document.getElementById("imageQuestion").files[0];
    const reader = new FileReader();
    let base64 = ""
    reader.addEventListener(
        "load",
        () => {
            base64 = reader.result;
        },
        false,
    );
    if (file){
        reader.readAsDataURL(file);
        return base64;
    }
}

function saveQuestionData() {
    if (formValidation(currForm.elements) && formValidation(configs.elements) && document.getElementById("fileName").value != ''){
        formToJson(currForm.elements, configs.elements);
        addQuestionBtn.disabled = true;
        newQuestionBtn.disabled = false;
        submitBtn.disabled = false;
    }
}

function changeForm(){
    for (let form of forms.keys){
        if (form == questionType.options[questionType.selectedIndex].value){
            forms[form].hidden = false;
            currForm = forms[form]
        }else{
            forms[form].reset()
            forms[form].hidden = true;
        }
    }
}

function newQuestion(){
    currForm.reset();
    configs.reset();
    addQuestionBtn.disabled = false;
    newQuestionBtn.disabled = true;
    submitBtn.disabled = true;
}

function handleSubmit(){
    fileName = document.getElementById("fileName").value;
    questions["filename"] = fileName;
    var quiz = JSON.stringify(questions);
    
    // downloading to user device
    var file = new Blob([quiz], {type: "application/json"});
    download = URL.createObjectURL(file);
    const a = Object.assign(document.createElement("a"), {
        href: download,
        download: fileName,
        hidden: true
    });
    document.body.appendChild(a);
    a.click();
    URL.revokeObjectURL(download);
    a.remove()
    //window.location.replace("http://127.0.0.1:5000/user/portal")

    //sending to server (flask)
    var xhr = new XMLHttpRequest;
    // don't like that the link is hard coded here
    xhr.open('POST', 'http://127.0.0.1:5000/user/store_quiz', true);
    xhr.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    xhr.send(quiz);
}

questionType.addEventListener("change", changeForm);
addQuestionBtn.onclick=saveQuestionData;
newQuestionBtn.onclick=newQuestion;