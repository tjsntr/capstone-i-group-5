from flask import Blueprint, render_template, request, redirect, session

import json

from ..db.db import get_db


class Question:
    def __init__(self, text, options, correct_option):
        self.text = text
        self.options = options
        self.correct_option = correct_option


class Quiz:
    def __init__(self, teacher_code, quiz):
        self.teacher_code = teacher_code
        self.questions = self.generateQuestions(quiz)
        self.iter = iter(self.questions)     

    def generateQuestions(self, questions):
        questions = []
        for key, entry in questions.keys():
            self.questions.append(
                Question(
                    text=key,
                    options=entry["options"],
                    correct_option=entry["answer"]
                )
            )
        return questions


def get_questions():
    # {
    # "What is 1 + 3?":
    #     {"answer": 4, "options": [2, 3, 13, 4]}
    # ,
    # "What is 64 / 8?":
    #     {"answer": 8, "options": [4, 6, 8, 7]}
    # ,
    # "What is the secret to life?":
    #     {"answer": ["42", "money"], "options": ["money", "love", "42"]}
    # }
    db = get_db()
    session_row = db.execute("SELECT * FROM session WHERE id = ?", (session["teacher_code"],)).fetchone()
    return json.loads(session_row["questions"])


bp = Blueprint("game", __name__, url_prefix="/game")


@bp.route("/questions", methods=["GET", "POST"])
def questions_page():
    if "teacher_code" not in session:
        return redirect("/student")
    db = get_db()
    # get game from teacher_code (session_id)
    game_id = db.execute("SELECT game_id FROM session WHERE id = ?", (session["teacher_code"],)).fetchone()["game_id"]
    print("game_id", game_id)
    # if users point table for this sessions game does not exist, create it
    user_points = db.execute("SELECT * FROM points WHERE user_id = ? AND game_id = ?", (session["user_id"], game_id)).fetchone()
    print("user_points", user_points)
    if not user_points:
        print("Adding new user to points table")
        db.execute(
            "INSERT INTO points (user_id, game_id) VALUES (?, ?)",
            (session["user_id"], game_id),
        )
        db.commit()
    
    if "questions" not in session:
        # Fetch questions based on teacher_code
        print("old questions not found")
        session["questions"] = get_questions()
    
    q_list = [q for q in session["questions"]]
    
    if request.method == "POST":
        print("got submission")
        #print("User answered this option", request.form["answer"])
        quest_i = int(request.form["question_id"])-1
        quest = q_list[quest_i]
        print("User answered this question", quest)
        
        # # check if answer is correct
        # if request.form["option"] in answers:
        #     # add points to user
        #     db = get_db()
        #     db.execute(
        #         "UPDATE points SET points = points + 1 WHERE user_id = ? AND game_id = ?",
        #         (session["user_id"], game_id),
        #     )
        #     db.commit()
        
        # save answer to session
        session[quest] = request.form["option"]
        # check if last question
        print("quest_i", quest_i+1, "len(q_list)", len(q_list))
        if len(q_list) == quest_i+1:
            result = {}
            answered = []
            new_points = 0
            for q in q_list:
                correct = 0
                answers = session["questions"][q]["answer"]
                if not isinstance(answers, list):
                    answers = [str(answers)]
                else:
                    answers = [str(a) for a in answers]
                if session[q] in answers:
                    new_points+=1
                    correct = 1
                result[q] = "You answered " + session[q] + " Correct answer was/were " + str(session["questions"][q]["answer"])
                answered.append(correct)
                
                
            #update points and game table with what questions were answered.
            db = get_db()
            db.execute(
                "UPDATE points SET points = points + ? WHERE user_id = ? AND game_id = ?",
                (new_points, session["user_id"], game_id),
            )
            answered_old = db.execute(
                "SELECT answered FROM session WHERE id = ?", (session["teacher_code"])
            ).fetchone()
            db.commit()
            
            answered_old = answered_old["answered"].split(" ")
            print(answered_old)
            print(answered)
            answered_new = ""
            for x,i in enumerate(answered):
                answered_new+= str(int(answered_old[i]) + x) + " "
            db.execute(
                "UPDATE session SET answered = ? WHERE id = ?", (answered_new, session["teacher_code"])
            )
            db.commit()
            
            return render_template("game/show_results.html", results=result)
        
        print(session["questions"])
        return render_template("game/questions.html", question=q_list[quest_i+1],  question_data=session["questions"][q_list[quest_i+1]], question_id=quest_i+2)
    
    print("This should be question text", q_list[0])
    # send first question
    return render_template("game/questions.html",question=q_list[0], question_data=session["questions"][q_list[0]], question_id=1)


@bp.route("/quiz_view", methods=["GET", "POST"])
def quiz_view():
    db = get_db()
    # fetch all users points assiocated with the session
    print("Game id", session["game"]["id"],"Session id", session["session_id"])
    user_points_rows = db.execute(
        "SELECT * FROM points WHERE game_id = ?", (session["game"]["id"],)
    ).fetchall()
    user_points = sorted(
        [{key: row[key] for key in row.keys()} for row in user_points_rows],
        key=lambda x: x['points'],
        reverse=True  # Set reverse to True for descending order
        )[: session["game"]["num_display"]]
    users_data = []
    for user in user_points:
        user_data = db.execute(
            "SELECT * FROM user WHERE id = ?", (user["user_id"],)
        ).fetchone()
        users_data.append(
            {
                "name": user_data["username"],
                "points": user["points"]
            }
        )
    #get answered from session
    answered = db.execute(
        "SELECT answered FROM session WHERE id = ?", (session["session_id"],)
    ).fetchone()
    print(answered["answered"])
    return render_template(
        "game/quiz_view.html",
        session_id=session["session_id"],
        questions=session["quiz"],
        users_data=users_data,
    )
