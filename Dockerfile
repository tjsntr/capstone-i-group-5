# Use an official Python runtime as the parent image
FROM python:3.9-slim

# Set the working directory in the container to /app
WORKDIR /app/flask_app

# Install FISH shell
RUN apt-get update && apt-get install -y fish

# Install any needed packages specified in requirements.txt
RUN pip install black mypy flask

# Initialize the app's database
# RUN flask --app kahoot init-db

# Make port 5000 available to the world outside this container
EXPOSE 5000

# Define environment variable
ENV FLASK_APP=flask_app
ENV FLASK_RUN_HOST=0.0.0.0

# Uncomment below to run app on deployment of container
#COPY . /deploy
#WORKDIR /deploy/flask_app
#CMD ["flask", "--debug", "--app", "kahoot", "run"]