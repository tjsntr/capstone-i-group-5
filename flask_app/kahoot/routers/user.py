from flask import Flask, render_template, request, redirect, session, Blueprint, url_for

from ..db.db import get_db

import os, json

bp = Blueprint("user", __name__, url_prefix="/user")


@bp.route("/quest_board", methods=["GET", "POST"])
def quest_board():
    if request.method == "POST":
        # Validate teacher code and initialize session
        teacher_code = request.form.get("teacher_code")
        if teacher_code:
            session["teacher_code"] = teacher_code
            return redirect(url_for("game.questions_page"))
        else:
            return "Invalid code"
    return render_template("Landingpage.html")


@bp.route("/portal", methods=["GET", "POST"])
def portal():
    # grab all of users games
    db = get_db()
    old_games = db.execute(
        "SELECT * FROM game WHERE owner_id = ?", (session["user_id"],)
    ).fetchall()
    old_games_data = [{key: game[key] for key in game.keys()} for game in old_games]

    quiz_files = os.listdir("quizzes/")
    print(quiz_files)

    if request.method == "POST":
        db = get_db()
        # check if old game
        if request.form.get("game_name") in [game["name"] for game in old_games_data]:
            game = db.execute(
                "SELECT * FROM game WHERE name = ? AND owner_id = ?",
                (request.form.get("game_name"), session["user_id"]),
            ).fetchone()
            session["game"] = {key: game[key] for key in game.keys()}

            selected_file = request.form.get("quiz_file")
            # Handle the selected file
            with open(f"quizzes/{selected_file}") as file:
                session["quiz"] = json.load(file)
            
            blank = ""
            for q in session["quiz"]:
                blank = blank + "0 "

            # build a session
            db = get_db()
            result = db.execute(
                "INSERT INTO session (game_id, owner_id, questions, answered) VALUES (?, ?, ?, ?)",
                (session["game"]["id"], session["user_id"], json.dumps(session["quiz"]), blank),
            )
            db.commit()

            session["session_id"] = result.lastrowid
            return redirect(url_for("game.quiz_view"))
        # build a game and add to db if new game
        db.execute(
            "INSERT INTO game (name, owner_id, num_display) VALUES (?, ?, ?)",
            (
                request.form.get("game_name"),
                session["user_id"],
                request.form.get("num_display"),
            ),
        )
        db.commit()
        game = db.execute(
            "SELECT * FROM game WHERE name = ? AND owner_id = ?",
            (request.form.get("game_name"), session["user_id"]),
        ).fetchone()
        session["game"] = {key: game[key] for key in game.keys()}
        return redirect("/user/portal")

    return render_template("user/portal.html", old_games=old_games_data, quiz_files=quiz_files)


@bp.route("/select_quiz", methods=["GET", "POST"])
def select_quiz():
    if request.method == "POST":
        selected_file = request.form.get("quiz_file")
        # Handle the selected file
        with open(f"quizzes/{selected_file}") as file:
            session["quiz"] = json.load(file)
        
        blank = ""
        for q in session["quiz"]:
            blank = blank + "0"

        # build a session
        db = get_db()
        db.execute(
            "INSERT INTO session (game_id, owner_id, questions, answered) VALUES (?, ?, ?, ?)",
            (session["game"]["id"], session["user_id"], json.dumps(session["quiz"]), blank),
        )
        db.commit()
        return redirect(url_for("game.quiz_view"))
    else:
        # Get list of quiz files
        quiz_files = os.listdir("quizzes/")
        print(quiz_files)
        # Render template with quiz files
        return render_template("user/select_quiz.html", quiz_files=quiz_files)

@bp.route("/quiz_editor")
def quiz_editor():
    return render_template("user/quiz_editor.html")

@bp.route("/store_quiz", methods=["POST", "GET"])
def store_quiz():
    quiz = request.get_json()
    print(quiz)
    with open(f"quizzes/{quiz['filename']}.json", 'w') as file:
        json.dump(quiz, file)
    return render_template("user/portal.html")